
var MatchController = require('./matchController.js');
var db= require('../db/db');
var express= require('express');
var jwtauth = require('../helpers/jwt-verify')
var crypto = require('crypto');
var convert=require("xml-js");
var js2xmlparser = require("js2xmlparser");

var router = express.Router();

router.get('/generate',MatchController.generateBracket)

  router.route('/')
    .get(MatchController.allMatches)
    

    router.post('/update',MatchController.updateMatch);



module.exports = router;


